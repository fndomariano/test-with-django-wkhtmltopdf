from django.shortcuts import render
from django.views.generic.base import View
from wkhtmltopdf.views import PDFTemplateResponse

class MyPDF(View):
    filename = 'my_pdf.pdf'
    template_name = 'my_template.html'
    context={'title': 'Testis'}
    cmd_options = {
        'margin-top': 3,
    }

    def get(self, request):
        response = PDFTemplateResponse(request=request,
                                       template=self.template_name,
                                       context=self.context,
                                       cmd_options=self.cmd_options
                                       )
        return response



