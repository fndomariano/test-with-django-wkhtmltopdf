from django.conf.urls import patterns, include, url
from django.conf import settings
from core.views import *
from wkhtmltopdf.views import PDFTemplateView


urlpatterns = patterns('',
    url(r'^$', MyPDF.as_view(), name='pdf')
)
